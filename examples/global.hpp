#pragma once

enum class O_Flag {
    None,
    Divide,
    Delete,
    Collapse,
    Split,
    Flip,
    DivideTriangle,
    SelectTriangle
};

enum class S_Flag {
    None,
    BoundaryEdge,
    BoundaryHalfedge,
    BoundaryVertex
};

enum class S_Item {
    None,
    Point,
    Line,
    Triangle,
};