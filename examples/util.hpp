﻿#pragma

inline static auto uRGB(unsigned long r, unsigned long g, unsigned long b, unsigned long a = 255) -> unsigned long {
    return r | g << 8 | b << 16 | a << 24;
};
