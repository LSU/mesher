#pragma once
#include <pmp/SurfaceMesh.h>

class Line2D {
public:
    Line2D() = default;
    Line2D(const pmp::Point &p1, const pmp::Point &p2) {
        this->_p1 = p1;
        this->_p2 = p2;
    }

    pmp::Point p1() const {
        return _p1;
    }

    pmp::Point p2() const {
        return _p2;
    }

    pmp::Point D0(double u) const {
        return (1-u)* _p1 +u* _p2;
    }
private:
    pmp::Point _p1;
    pmp::Point _p2;
};